# SAE_IMAGE



## A0

 display a affiché un message d'erreur dû au faite que l'image avait un manque d'un octet j'ai donc rajouté un octet pour résoudre  le   problème 
 image 1 et 2

![](A.0probleme.png)
## A1
![](2.PNG) ![](3.PNG)
## A2
![](4.PNG) ![](5.PNG)
## A3
![](6.PNG) Image1.bmp fait 102 octets après la conversion.

1) ![](7.PNG) ![](8.PNG) 
Dans Image0.bmp, il y a 24 bits par pixel, Image1.bmp possède le même nombre de bits par pixel.

2) La taille des données pixels correspond au ‘66’ qui est codé juste après ‘42 4D’. Si on convertit cette donnée en décimal, on obtient la taille du fichier soit : 102 octets.

3) Aucune compression n’est utilisée puisque la taille du fichier est égale à la donnée du troisième bit que l’on peut consulter sur Okteta.

4)Les pixels sont toujours codés en little-endian, donc le codage n’a pas été changé.

## A4
![](9.PNG)
1)
![](10.PNG) ![](11.PNG) Image1.bmp possède 24 bits par pixel. Image2.bmp possède

2)
La taille des données pixels correspond au ‘4E’ qui est codé juste après ‘42 4D’. Si on convertit cette donnée en décimal, on obtient la taille du fichier soit : 78 octets.

3)
Aucune compression n’est utilisée puisque la taille du fichier est égale à la donnée du troisième bit que l’on peut consulter sur Okteta.

4)
Les couleurs de la palette sont codées en RGB.

5)
Il y a 256 couleurs disponibles dans la palette.

6)

7)
![](12.PNG) ![](13.PNG)


8)


![](14.PNG) ![](15.PNG)


9)


![](16.PNG) ![](17.PNG)



11)
On peut trouver le nombre de couleurs disponibles dans la palette à l’adresse : 0x2E.
12)
La couleur dominante « Blanc » est présente à l’adresse 0x32.

13)
Le tableau de pixel commence à l’adresse 0xCA.

14)
![](18.PNG) 

15)![](19.PNG)
## A5
L’image s’inverse si on change la valeur 4 par -4.
![](20.PNG)

## A6
Le fichier Image4.bmp fait 1 120 octets.

![](21.PNG) 

L’offset qui donne l’adresse de début des pixels correspond à la partie sélectionnée en bleue. Donc les pixels débutent à l’adresse 0x436.

Si on décode les pixels, on trouve : 00 01 00 01 01 00 01 00 00 01 00 01 01 00 01 00.

## A7
Le poids du fichier Image5.bmp est de 1 102 octets.

Si on décode les pixels, on trouve : 00 01 00 01 01 00 01 00 00 01 00 01 01 00 01 00.


## A8

![](22.PNG) ![](23.PNG)

